/*
 * Copyright (c) 2017 Leonardo Baldin e collaboratori.
 *
 * Il progetto Dallaz e questo file CustomTextPanel.java di esso facente parte sono sottoposti alla licenza GNU GPLv3, di cui una
 * copia è il file LICENSE.
 * Per i termini della licenza, questa deve essere mantenuta tale ed è obbligatorio il riconoscimento degli autori originali
 * ad ogni utilizzo di questo codice.
 *
 * QUESTO CODICE È PROVVISTO "COSÌ COM'È", PRIVO DI TASSAZIONE O RESTRIZIONE DI UTILIZZO COMMERCIALE.
 * NÈ L'AUTORE PRIMO, NÈ I COLLABORATORI SONO DA RITENERSI RESPONSABILI PER UN UTILIZZO IMPROPRIO DEL CODICE
 * DA PARTE DI TERZI.
 */

package UI.Frames;

import Utilities.Resource;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

/**
 * Created by Leonardo Baldin on 02/05/17.
 */

public class CustomTextPanel extends JPanel {

    private int charCounter = 0;
    private String testo;

    private Clip typewriterSound;
    private Timer timer = null;

    private static final Color OVERLAY_COLOR = new Color(0, 0, 0, 120);

    private JTextArea testoh = new JTextArea();
    private JFrame root;
    private EmptyBorder border = null;

    private static final String FONT_FILE = "Resources/Bohemian_Typewriter.ttf";
    private Color FONT_COLOR = Color.WHITE;
    private float fontSize = 21f;
    private Font font;

    private Image BACKGROUND_IMAGE = null;
    private String IMAGE_PATH = "Resources/background1.jpeg";

    public CustomTextPanel(@NotNull JFrame rootFrame, @Nullable String testo) {
        super();
        root = rootFrame;
        this.testo = testo;

        try {
            BACKGROUND_IMAGE = Resource.getResource(Image.class, IMAGE_PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }

        border = new EmptyBorder(new Insets(root.getHeight()/6, root.getWidth()/7, 0, root.getWidth()/7));
        this.setBorder(border);
        this.setLayout(new BorderLayout());
        _initTesto();
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                removeMouseListener(this);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    public CustomTextPanel(@Nullable String testo) {
        super();
        this.testo = testo;

        try {
            BACKGROUND_IMAGE = Resource.getResource(Image.class, IMAGE_PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }

        border = new EmptyBorder(new Insets(getHeight()/8, getWidth()/9, 0, getWidth()/9));
        this.setBorder(border);
        this.setLayout(new BorderLayout());
        _initTesto();
    }

    private void _initTesto() {
        testoh.setForeground(FONT_COLOR);

        testoh.setEditable(false);
        testoh.getCaret().deinstall(testoh);
        testoh.setLineWrap(true);
        testoh.setWrapStyleWord(true);
        testoh.setOpaque(false);
        testoh.setAlignmentX(SwingConstants.LEFT);

        add(testoh, BorderLayout.CENTER);

        try {
            font = Resource.getResource(Font.class, FONT_FILE);
            font = font.deriveFont(Font.PLAIN, fontSize);
            testoh.setFont(font);
        } catch (Exception e) {
            e.printStackTrace();
        }

        _initTimer();
        this.addPropertyChangeListener(propertyChange -> _animateText());
        /*this.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                _animateText(testo);
            }

            @Override
            public void focusLost(FocusEvent e) {
                _animateText(testo);
            }
        });*/
    }

    public void addMouseListener(MouseListener listener) {
        testoh.addMouseListener(listener);
    }

    private void _animateText() {

        if (!timer.isRunning()){
            playKeystrokeSound();
            timer.start();
        } else {
            stopKeystrokeSound();
            timer.stop();
        }
    }

    private void _initTimer() {
        timer = new Timer(35, actionEvent -> {
            if (charCounter < testo.length()) {
                testoh.append(Character.toString(testo.charAt(charCounter)));
                charCounter++;
            } else {
                ((Timer) actionEvent.getSource()).stop();
                stopKeystrokeSound();
            }
        });
        timer.setCoalesce(true);
        timer.setRepeats(true);
    }

    // POTREBBE DARE ERRORI
    public void endAnimation() {
        stopKeystrokeSound();
        timer.stop();
        testoh.setText(null);
        testoh.setText(testo);
    }

    private void playKeystrokeSound() {
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(Resource.getStream("Resources/typewriter.wav"));
            Clip clip = AudioSystem.getClip();
            clip.open(audioStream);
            clip.start();
            clip.loop(6);
            typewriterSound = clip;
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            System.out.println("Error with playing sound.");
        }
    }

    private void stopKeystrokeSound() {
        typewriterSound.stop();
        typewriterSound.close();
    }

    public void setBackgroundImage(String path) {
        IMAGE_PATH = path;
        try {
            BACKGROUND_IMAGE = Resource.getResource(Image.class, IMAGE_PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }
        repaint();
    }

    public void setFontSize(float newSize) {
        fontSize = newSize;
        font = font.deriveFont(fontSize);
        testoh.setFont(font);
    }

    public void setNewText(String newText) {
        testoh.setText(newText);
    }

    public void setTextColor(Color newColor) {
        FONT_COLOR = newColor;
        testoh.setForeground(newColor);
    }

    public String getTesto() {
        return this.testo;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Polygon fill = new Polygon();
        fill.addPoint(getX(), getY());
        fill.addPoint(this.getWidth(), getY());
        fill.addPoint(this.getWidth(), this.getHeight());
        fill.addPoint(getX(), this.getHeight());
        g.drawImage(BACKGROUND_IMAGE, 0, 0, null);
        g.setColor(OVERLAY_COLOR);
        g.fillPolygon(fill);
    }
}
