/*
 * Copyright (c) 2017 Leonardo Baldin e collaboratori.
 *
 * Il progetto Dallaz e questo file MainMenuPanel.java di esso facente parte sono sottoposti alla licenza GNU GPLv3, di cui una
 * copia è il file LICENSE.
 * Per i termini della licenza, questa deve essere mantenuta tale ed è obbligatorio il riconoscimento degli autori originali
 * ad ogni utilizzo di questo codice.
 *
 * QUESTO CODICE È PROVVISTO "COSÌ COM'È", PRIVO DI TASSAZIONE O RESTRIZIONE DI UTILIZZO COMMERCIALE.
 * NÈ L'AUTORE PRIMO, NÈ I COLLABORATORI SONO DA RITENERSI RESPONSABILI PER UN UTILIZZO IMPROPRIO DEL CODICE
 * DA PARTE DI TERZI.
 */

package UI.Frames;

import UI.Elements.CustomButton;
import Utilities.ImageResizer;
import Utilities.Resource;
import com.sun.istack.internal.NotNull;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.BufferedInputStream;
import java.io.IOException;

/**
 * Created by Leonardo Baldin on 28/04/17.
 */


public class MainMenuPanel extends JPanel {

    private static Image BACKGROUND_IMAGE;
    private static final String IMAGE_PATH = "Resources/background.jpeg";

    private JFrame root = null;

    private static Clip mainMenuMusic;

    public CustomButton btnGioca;
    public CustomButton btnCrediti;

    private Polygon side = new Polygon();

    public MainMenuPanel(@NotNull JFrame rootFrame) {
        super();
        this.root = rootFrame;

        setOpaque(false);
        side.addPoint(0, 0);
        side.addPoint(root.getWidth() /5, 0);
        side.addPoint(root.getWidth() /7, root.getHeight());
        side.addPoint(0, root.getHeight());

        try {
            BACKGROUND_IMAGE = Resource.getResource(Image.class, IMAGE_PATH);
            BACKGROUND_IMAGE = ImageResizer.resizeImage(BACKGROUND_IMAGE, root.getWidth(), root.getHeight(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        _initPanel();
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(BACKGROUND_IMAGE, 0, 0, null);
        g.setColor(new Color(0, 0, 0, 175));
        g.fillPolygon(side);
        super.paint(g);
    }

    private void _initPanel() {
        btnGioca = new CustomButton(root.getWidth()/5, root.getHeight()/8, "Gioca");
        btnCrediti = new CustomButton(root.getWidth()/6, root.getHeight()/11, "Crediti");
        CustomButton btnEsci = new CustomButton(root.getWidth()/6, root.getHeight()/11, "Esci");

        musicStart();

        btnGioca.setFontSize(50f);
        btnEsci.setFontSize(30f);
        btnCrediti.setFontSize(30f);

        btnEsci.addActionListener(e -> System.exit(0));

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.linkSize(SwingConstants.HORIZONTAL, btnCrediti, btnEsci);
        layout.linkSize(SwingConstants.VERTICAL, btnCrediti, btnEsci);

        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(btnGioca, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(root.getWidth()/20)
                        .addComponent(btnCrediti)
                        .addComponent(btnEsci)
        );

        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(btnGioca, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup())
                            .addComponent(btnCrediti)
                            .addComponent(btnEsci)
        );

        this.setBorder(new EmptyBorder(new Insets(root.getHeight()/18, root.getWidth()/20, 0, 0)));
    }

    public void musicStart() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new BufferedInputStream(Resource.getStream("Resources/mainMenuSong.wav")));
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            FloatControl gain = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gain.setValue(-10.0f);
            clip.start();
            clip.loop(6);
            mainMenuMusic = clip;
        } catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
            System.out.println("Errore play musica!");
        }
    }

    public void stopMusic(){
        mainMenuMusic.stop();
        mainMenuMusic.close();
    }
}
