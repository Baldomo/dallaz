/*
 * Copyright (c) 2017 Leonardo Baldin e collaboratori.
 *
 * Il progetto Dallaz e questo file MainFrame.java di esso facente parte sono sottoposti alla licenza GNU GPLv3, di cui una
 * copia è il file LICENSE.
 * Per i termini della licenza, questa deve essere mantenuta tale ed è obbligatorio il riconoscimento degli autori originali
 * ad ogni utilizzo di questo codice.
 *
 * QUESTO CODICE È PROVVISTO "COSÌ COM'È", PRIVO DI TASSAZIONE O RESTRIZIONE DI UTILIZZO COMMERCIALE.
 * NÈ L'AUTORE PRIMO, NÈ I COLLABORATORI SONO DA RITENERSI RESPONSABILI PER UN UTILIZZO IMPROPRIO DEL CODICE
 * DA PARTE DI TERZI.
 */

package UI.Frames;

import GruppoGUI.FinestraDomanda;
import Utilities.Resource;
import Utilities.StoryNode;
import com.sun.istack.internal.Nullable;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Objects;

/**
 * Created by Leonardo Baldin on 29/04/17.
 */

public class MainFrame {

    private JFrame root = new JFrame();

    private MainMenuPanel menu;
    private FinestraDomanda currentChoicesPanel;
    private CustomTextPanel currentStoryPanel;

    private StoryManager storyManager = new StoryManager();

    private Image icon = null;

    private final String CREDITI =
            "GRAFICA - Marco Pozzebon, Dafne Barbera, Leonardo Baldin, Alessandro Jingberger " +
            "\n\nPROGRAMMAZIONE - Leonardo Baldin, Emanuele Cason, Emma Dalla Mora, Francesco Bettiol, Gioele Bisetto, Brando Zanin, Riccardo Conte" +
            "\n\nAUDIO - Giacomo Voltarel" +
            "\n\nSTORIA - Andrea Zaffalon, Emma Dalla Mora, Federico De Marchi, Chiara Nappa, Elisa Chiappa" +
            "\n\n\nDEDICATO A DAVIDE DALLA VALLE";

    public MainFrame(@Nullable String title) {
        super();

        SwingUtilities.invokeLater(() -> {
            root.setTitle(title);
            root.setResizable(false);
            root.setSize(1280, 720);
            root.setLocationRelativeTo(null);
            root.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            //root.setExtendedState(JFrame.MAXIMIZED_BOTH);
            //root.setUndecorated(true);
            _initIcon();
        });
        loadMainMenu();
        root.setVisible(true);

    }

    private void _initIcon() {
        try {
            icon = Resource.getResource(Image.class, "Resources/icon.png");
        } catch (Exception e) {
            e.printStackTrace();
        }
        root.setIconImage(icon);
    }

    public void loadMainMenu() {
        SwingUtilities.invokeLater(() -> {
            root.getContentPane().removeAll();
            root.getContentPane().invalidate();

            menu = new MainMenuPanel(root);

            root.getContentPane().add(menu);
            root.getContentPane().revalidate();
            CustomTextPanel first = new CustomTextPanel(root, storyManager.getCurrentStoryNode().getFirstStory());
            first.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    startMainLoop();
                }

                @Override
                public void mousePressed(MouseEvent e) {}

                @Override
                public void mouseReleased(MouseEvent e) {}

                @Override
                public void mouseEntered(MouseEvent e) {}

                @Override
                public void mouseExited(MouseEvent e) {}
            });

            menu.btnGioca.addActionListener(e -> {
                menu.stopMusic();
                swapToTextPanel(first);
            });

            menu.btnCrediti.addActionListener(e -> {
                menu.stopMusic();
                swapToTextPanel(new CustomTextPanel(root, CREDITI));
            });

        });
    }

    public void startMainLoop() {
        SwingUtilities.invokeLater(() -> {
            currentChoicesPanel = new FinestraDomanda(root, storyManager.getCurrentStoryNode());
            swapToChoicePanel(currentChoicesPanel);
            currentChoicesPanel.addPropertyChangeListener(change -> {
                if (Objects.equals(change.getPropertyName(), "responso")) {
                    currentStoryPanel = storyManager.loadNextStory(currentChoicesPanel.getResponso());
                    currentStoryPanel.addMouseListener(new MouseListener() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            if (!storyManager.checkEOF()) {
                                currentChoicesPanel = storyManager.loadNextDomanda(storyManager.getCurrentStoryNode(), currentChoicesPanel.getResponso());
                                swapToChoicePanel(currentChoicesPanel);
                            } else {
                                swapToTextPanel(new CustomTextPanel(root, "Fine."));
                            }

                        }

                        @Override
                        public void mousePressed(MouseEvent e) {

                        }

                        @Override
                        public void mouseReleased(MouseEvent e) {

                        }

                        @Override
                        public void mouseEntered(MouseEvent e) {

                        }

                        @Override
                        public void mouseExited(MouseEvent e) {

                        }
                    });
                    swapToTextPanel(currentStoryPanel);
                }
            });
        });
    }

    public void swapToChoicePanel(FinestraDomanda newPanel) {
        SwingUtilities.invokeLater(() -> {
            root.getContentPane().removeAll();
            root.getContentPane().invalidate();
            newPanel.addPropertyChangeListener(change -> {
                if (Objects.equals(change.getPropertyName(), "responso")) {
                    if (!storyManager.checkEOF()) {
                        currentStoryPanel = storyManager.loadNextStory(currentChoicesPanel.getResponso());
                        swapToTextPanel(currentStoryPanel);
                    } else swapToTextPanel(new CustomTextPanel(root, storyManager.getLastStory()));
                }
            });
            root.getContentPane().add(newPanel);
            root.getContentPane().revalidate();
        });
    }

    private void swapToTextPanel(CustomTextPanel newPanel) {
        SwingUtilities.invokeLater(() -> {
            root.getContentPane().removeAll();
            root.getContentPane().invalidate();
            newPanel.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (newPanel.getTesto().equals(CREDITI) || newPanel.getTesto().equals(storyManager.getLastStory())) {
                        loadMainMenu();
                    } else {
                        currentChoicesPanel = storyManager.loadNextDomanda(storyManager.getCurrentStoryNode(), currentChoicesPanel.getResponso());
                        swapToChoicePanel(currentChoicesPanel);
                    }

                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });
            root.getContentPane().add(newPanel);
            root.getContentPane().revalidate();
        });
    }


    public class StoryManager {

        private StoryNode currentStoryNode;

        public StoryManager() {
            currentStoryNode = new StoryNode().getFirstStoryNode();
        }

        public FinestraDomanda loadNextDomanda(StoryNode storyNode, int scelta) {
            currentStoryNode = storyNode.getNextStoryNode(scelta);
            return new FinestraDomanda(root, currentStoryNode);
        }

        public CustomTextPanel loadNextStory(int scelta) {
            return new CustomTextPanel(root, currentStoryNode.getNextStoryText(scelta));
        }

        public boolean checkEOF() {
            if (currentStoryNode.getNextIndex1() == 999) return true;
            if (currentStoryNode.getNextIndex2() == 999) return true;
            return false;
        }

        public int getLastStoryIndex() {
            return currentStoryNode.getLastStoryIndex();
        }

        public int getLastChoiceIndex() {
            return currentStoryNode.getLastChoiceIndex();
        }

        public String getLastStory() {
            return currentStoryNode.getStory(getLastStoryIndex());
        }

        public StoryNode getCurrentStoryNode() {
            return currentStoryNode;
        }

    }
}
