/*
 * Copyright (c) 2017 Leonardo Baldin e collaboratori.
 *
 * Il progetto Dallaz e questo file ChoicesPanel.java di esso facente parte sono sottoposti alla licenza GNU GPLv3, di cui una
 * copia è il file LICENSE.
 * Per i termini della licenza, questa deve essere mantenuta tale ed è obbligatorio il riconoscimento degli autori originali
 * ad ogni utilizzo di questo codice.
 *
 * QUESTO CODICE È PROVVISTO "COSÌ COM'È", PRIVO DI TASSAZIONE O RESTRIZIONE DI UTILIZZO COMMERCIALE.
 * NÈ L'AUTORE PRIMO, NÈ I COLLABORATORI SONO DA RITENERSI RESPONSABILI PER UN UTILIZZO IMPROPRIO DEL CODICE
 * DA PARTE DI TERZI.
 */

package UI.Frames;

import UI.Elements.CustomButton;
import Utilities.ImageResizer;
import Utilities.StoryNode;
import com.sun.istack.internal.NotNull;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * Created by Leonardo Baldin on 29/04/17.
 */

public class ChoicesPanel extends JPanel {

    private StoryNode storyNode;

    private JFrame root;

    public CustomButton btn1;
    public CustomButton btn2;
    private CustomTextPanel testoh;

    private static final String FONT_FILE = "Resources/Cutrims.otf";
    private Color FONT_COLOR = Color.WHITE;
    private float fontSize = 36f;
    private Font font;

    private Image BACKGROUND_IMAGE = null;
    private String IMAGE_NAME = "/Resources/saloon.png";

    public ChoicesPanel(@NotNull JFrame rootFrame, StoryNode storyNode) {
        super();
        this.storyNode = storyNode;
        root = rootFrame;

        try {
            BACKGROUND_IMAGE = ImageIO.read(getClass().getResource(IMAGE_NAME));
            BACKGROUND_IMAGE = ImageResizer.resizeImage(BACKGROUND_IMAGE, root.getWidth(), root.getHeight(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        _initText();
        _initButtons();
        _initPanel();
    }

    private void _initText() {
        testoh = new CustomTextPanel(storyNode.getDomanda());
        testoh.setBackground(new Color(0, 0, 0, 175));
    }

    private void _initButtons() {
        btn1 = new CustomButton(root.getWidth()/5, root.getHeight()/8, storyNode.getScelta1());
        btn2 = new CustomButton(root.getWidth()/5, root.getHeight()/8, storyNode.getScelta2());
    }

    private void _initPanel() {
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.linkSize(SwingConstants.HORIZONTAL, btn1, btn2);
        layout.linkSize(SwingConstants.VERTICAL, btn1, btn2);

        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(testoh)
                        .addGroup(layout.createParallelGroup())
                        .addComponent(btn1)
                        .addComponent(btn2)
        );

        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addComponent(testoh)
                        .addGroup(layout.createSequentialGroup())
                        .addComponent(btn1)
                        .addComponent(btn2)
        );

    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(BACKGROUND_IMAGE, 0, 0, null);
    }

}
