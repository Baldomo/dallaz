/*
 * Copyright (c) 2017 Leonardo Baldin e collaboratori.
 *
 * Il progetto Dallaz e questo file RandomImageLoader.java di esso facente parte sono sottoposti alla licenza GNU GPLv3, di cui una
 * copia è il file LICENSE.
 * Per i termini della licenza, questa deve essere mantenuta tale ed è obbligatorio il riconoscimento degli autori originali
 * ad ogni utilizzo di questo codice.
 *
 * QUESTO CODICE È PROVVISTO "COSÌ COM'È", PRIVO DI TASSAZIONE O RESTRIZIONE DI UTILIZZO COMMERCIALE.
 * NÈ L'AUTORE PRIMO, NÈ I COLLABORATORI SONO DA RITENERSI RESPONSABILI PER UN UTILIZZO IMPROPRIO DEL CODICE
 * DA PARTE DI TERZI.
 */

package Utilities;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Leonardo Baldin on 30/05/17.
 */
public final class RandomImageLoader {

    private static ArrayList<Image> images = new ArrayList<>();

    private static final ArrayList<String> exts = new ArrayList<>(Arrays.asList(".bmp", ".png", ".jpg", ".jpeg"));

    public static void addImage(String path) {
        try {
            images.add(Resource.getResource(Image.class, path));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addAll(String[] filenames) {
        for (String filename : filenames) {
            if (!exts.contains(filename.substring(filename.lastIndexOf("."))))
                throw new IllegalStateException("Estensione non supportata!" + filename.substring(filename.lastIndexOf(".")));
            else {
                try {
                    images.add(Resource.getResource(Image.class, filename));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Image getRandomImage() throws InstantiationException {
        if (images.isEmpty()) throw new InstantiationException("Nessuna immagine caricata!");
        return images.get(ThreadLocalRandom.current().nextInt(images.size()));
    }
}
