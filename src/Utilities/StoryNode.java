/*
 * Copyright (c) 2017 Leonardo Baldin e collaboratori.
 *
 * Il progetto Dallaz e questo file StoryNode.java di esso facente parte sono sottoposti alla licenza GNU GPLv3, di cui una
 * copia è il file LICENSE.
 * Per i termini della licenza, questa deve essere mantenuta tale ed è obbligatorio il riconoscimento degli autori originali
 * ad ogni utilizzo di questo codice.
 *
 * QUESTO CODICE È PROVVISTO "COSÌ COM'È", PRIVO DI TASSAZIONE O RESTRIZIONE DI UTILIZZO COMMERCIALE.
 * NÈ L'AUTORE PRIMO, NÈ I COLLABORATORI SONO DA RITENERSI RESPONSABILI PER UN UTILIZZO IMPROPRIO DEL CODICE
 * DA PARTE DI TERZI.
 */

package Utilities;

import Json.JSONArray;
import Json.JSONObject;
import Json.JSONTokener;

import java.io.IOException;

/**
 * Created by Leonardo Baldin on 30/04/17.
 */

public class StoryNode {

    private String domanda;
    private String scelta1;
    private String scelta2;

    private int nextIndex1;
    private int nextIndex2;

    private int storyIndex1;
    private int storyIndex2;

    public String getDomanda() {
        return domanda;
    }

    public String getScelta1() {
        return scelta1;
    }

    public String getScelta2() {
        return scelta2;
    }

    public int getNextIndex1() {
        return nextIndex1;
    }

    public int getNextIndex2() {
        return nextIndex2;
    }

    public int getStoryIndex1() {
        return storyIndex1;
    }

    public int getStoryIndex2() {
        return storyIndex2;
    }

    public StoryNode(String domanda, String scelta1, String scelta2, int nextIndex1, int nextIndex2, int storyIndex1, int storyIndex2) {
        this.domanda = domanda;
        this.scelta1 = scelta1;
        this.scelta2 = scelta2;
        this.nextIndex1 = nextIndex1;
        this.nextIndex2 = nextIndex2;
        this.storyIndex1 = storyIndex1;
        this.storyIndex2 = storyIndex2;
    }

    public StoryNode() {}

    @Override
    public String toString() {
        return "Domanda: " + domanda;
    }


    public StoryNode getNextStoryNode(int scelta) {
        JSONObject jsonChoice = null;
        if (scelta == 1) {
            jsonChoice = new Json().getChoices().getJSONObject(getNextIndex1());
        } else if (scelta == 2) {
            jsonChoice = new Json().getChoices().getJSONObject(getNextIndex2());
        } else throw new IndexOutOfBoundsException("int scelta non è né 1 né 2!");

        return new StoryNode(
                jsonChoice.getString("domanda"),
                jsonChoice.getString("scelta1"),
                jsonChoice.getString("scelta2"),
                jsonChoice.getInt("nextIndex1"),
                jsonChoice.getInt("nextIndex2"),
                jsonChoice.getInt("storyIndex1"),
                jsonChoice.getInt("storyIndex2")
        );
    }

    public String getNextStoryText(int scelta) {
        if (scelta == 1) {
            return new Json().getStories().getString(getStoryIndex1());
        } else if (scelta == 2) {
            return new Json().getStories().getString(getStoryIndex2());
        } else throw new IndexOutOfBoundsException("int scelta non è né 1 né 2!");

    }

    public StoryNode getFirstStoryNode() {
        return new Json().getStoryNode(0);
    }

    public String getFirstStory() {
        return new Json().getStories().getString(0);
    }

    public StoryNode getStoryNode(int index) {
        return new Json().getStoryNode(index);
    }

    public String getStory(int index) {
        return new Json().getStory(index);
    }

    public int getLastStoryIndex() {
        return new Json().lastStoryIndex();
    }

    public int getLastChoiceIndex() {
        return new Json().lastChoiceIndex();
    }

    /**
     * La classe interna Json (è anche statica!) serve a ottenere singoli oggetti da story.json
     * e convertirli in oggetti StoryNode, contenenti la domanda e le due scelte sotto forma di stringhe.
     */
    private class Json {

        private static final String JSON_FILE = "Resources/story.json";
        private JSONTokener tokener = null;
        private JSONObject story;
        private JSONArray choices;
        private JSONArray stories;

        public Json() {
            reload();
        }

        private void reload() {
            // tokener è un oggetto che legge il file json
            try {
                tokener = new JSONTokener(Resource.getStream(JSON_FILE));
            } catch (IOException e) {
                System.out.println("Errore caricamento JSON!");
            }

            // Il file viene trasformato in un oggetto per facilitare accesso ed estrazione delle scelte
            story = new JSONObject(tokener);
            // getJSONArray estrae dall'oggetto json l'array "choices"
            choices = story.getJSONArray("choices");
            stories = story.getJSONArray("stories");
        }

        /**
         * getChoice legge dall'array all'indice indicato e crea un oggetto StoryNode che contiene i valori estratti
         * dal json
         */
        public StoryNode getStoryNode(int index) {
            reload();
            JSONObject jsonChoice = choices.getJSONObject(index);
            return new StoryNode(
                    jsonChoice.getString("domanda"),
                    jsonChoice.getString("scelta1"),
                    jsonChoice.getString("scelta2"),
                    jsonChoice.getInt("nextIndex1"),
                    jsonChoice.getInt("nextIndex2"),
                    jsonChoice.getInt("storyIndex1"),
                    jsonChoice.getInt("storyIndex2")
            );
        }

        public String getStory(int index) {
            return stories.getString(56);
        }

        public JSONArray getChoices() {
            return choices;
        }

        public JSONArray getStories() {
            return stories;
        }

        public int lastStoryIndex() {
            return stories.length()-1;
        }

        public int lastChoiceIndex() {
            return choices.length()-1;
        }
    }

}
