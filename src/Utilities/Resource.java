/*
 * Copyright (c) 2017 Leonardo Baldin e collaboratori.
 *
 * Il progetto Dallaz e questo file Resource.java di esso facente parte sono sottoposti alla licenza GNU GPLv3, di cui una
 * copia è il file LICENSE.
 * Per i termini della licenza, questa deve essere mantenuta tale ed è obbligatorio il riconoscimento degli autori originali
 * ad ogni utilizzo di questo codice.
 *
 * QUESTO CODICE È PROVVISTO "COSÌ COM'È", PRIVO DI TASSAZIONE O RESTRIZIONE DI UTILIZZO COMMERCIALE.
 * NÈ L'AUTORE PRIMO, NÈ I COLLABORATORI SONO DA RITENERSI RESPONSABILI PER UN UTILIZZO IMPROPRIO DEL CODICE
 * DA PARTE DI TERZI.
 */

package Utilities;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Created by Leonardo Baldin on 22/05/17.
 */


public final class Resource {
    private static Map<Class<?>, Loader<?>> loaders;
    private static Logger logger;

    public static <T> void registerLoader(Class<T> typeClass, Loader<T> loader) {
        loaders.put(typeClass, loader);
    }

    public static void registerLogger(Logger logger) {
        Resource.logger = logger;
    }

    public static InputStream getStream(String path) throws IOException {
        InputStream resourceStream = Resource.class.getClassLoader().getResourceAsStream(path);
        if(resourceStream != null) {
            //logger.info(String.format("Loading stream \"%s\" from resource.", path));
            return resourceStream;
        } else {
            //logger.info(String.format("Loading stream \"%s\" from file.", path));
            return new FileInputStream(path);
        }
    }

    public static <T> T getResourceFromStream(Class<T> typeClass, InputStream stream) throws Exception {
        if(loaders.containsKey(typeClass)) {
            return typeClass.cast(loaders.get(typeClass).load(stream));
        } else {
            throw new RuntimeException(String.format("No resource loader defined for type %s.", typeClass.getName()));
        }
    }

    public static <T> T getResource(Class<T> typeClass, String path) throws Exception {
        return getResourceFromStream(typeClass, getStream(path));
    }

    static {
        loaders = new HashMap<Class<?>, Loader<?>>();
        logger = Logger.getLogger("Resources");

        registerLoader(Image.class, new ImageLoader());
        registerLoader(Font.class, new FontLoader());
    }

    public interface Loader<T> {
        T load(InputStream stream) throws Exception;
    }

    public static class ImageLoader implements Loader<Image> {
        @Override
        public Image load(InputStream stream) throws IOException {
            Image image = ImageIO.read(stream);
            if (image == null) return null;
            else return image;
        }
    }

    public static class FontLoader implements Loader<Font> {
        @Override
        public Font load(InputStream stream) throws IOException, FontFormatException {
            return Font.createFont(Font.TRUETYPE_FONT, stream);
        }
    }
}
