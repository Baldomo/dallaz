/*
 * Copyright (c) 2017 Leonardo Baldin e collaboratori.
 *
 * Il progetto Dallaz e questo file FinestraDomanda.java di esso facente parte sono sottoposti alla licenza GNU GPLv3, di cui una
 * copia è il file LICENSE.
 * Per i termini della licenza, questa deve essere mantenuta tale ed è obbligatorio il riconoscimento degli autori originali
 * ad ogni utilizzo di questo codice.
 *
 * QUESTO CODICE È PROVVISTO "COSÌ COM'È", PRIVO DI TASSAZIONE O RESTRIZIONE DI UTILIZZO COMMERCIALE.
 * NÈ L'AUTORE PRIMO, NÈ I COLLABORATORI SONO DA RITENERSI RESPONSABILI PER UN UTILIZZO IMPROPRIO DEL CODICE
 * DA PARTE DI TERZI.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GruppoGUI;

import UI.Elements.CustomButton;
import Utilities.ImageResizer;
import Utilities.RandomImageLoader;
import Utilities.Resource;
import Utilities.StoryNode;
import com.sun.istack.internal.NotNull;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author marco
 */

public class FinestraDomanda extends JPanel {

    private int responso = 0;
    private String[] images = {
            "Resources/background1.jpeg",
            "Resources/background2.jpeg",
            "Resources/background3.jpeg",
            "Resources/background4.jpeg",
            "Resources/background5.jpeg"
    };

    private JTextArea domanda = new JTextArea();
    private CustomButton ris1 = new CustomButton(300, 100, "");
    private CustomButton ris2 = new CustomButton(300, 100, "");
    private JLabel mission = new JLabel();
    private ImageIcon backgroundImage;
    private JLabel background = new JLabel(backgroundImage);

    private Font stampact = null;
    private Font goldenage = null;

    private StoryNode storyNode;
    private PropertyChangeSupport propertyChangeSupport;

    public FinestraDomanda(@NotNull JFrame rootFrame, StoryNode storyNode) {
        _initFont();
        this.storyNode = storyNode;
        propertyChangeSupport = new PropertyChangeSupport(this);

        RandomImageLoader.addAll(images);
        try {
            backgroundImage = new ImageIcon(RandomImageLoader.getRandomImage());
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        domanda.setLineWrap(true);
        domanda.setWrapStyleWord(true);
        domanda.setEditable(false);
        domanda.getCaret().deinstall(domanda);

        setLayout(new BorderLayout());
        add(background);
        background.setLayout(null);

        mission.setFont(stampact);
        mission.setForeground(new Color(58, 32, 16));
        background.add(mission);
        mission.setBounds(74, 10, 972, 100);

        domanda.setBorder(null);
        domanda.setBackground(new Color(0, 0, 0, 100));
        domanda.setFont(goldenage);
        domanda.setForeground(new Color(241, 218, 194));
        background.add(domanda);
        domanda.setEditable(false);
        domanda.setBounds(74, 120, 972, 280);

        background.add(ris1);
        ris1.setBounds(74, 450, 300, 100);

        background.add(ris2);
        ris2.setBounds(746, 450, 300, 100);

        ris1.addActionListener(actionEvent -> {
            domanda.setText(ris1.getText());
            responso = 1;
            propertyChangeSupport.firePropertyChange("responso", 0, 1);
        });

        ris2.addActionListener(actionEvent -> {
            domanda.setText(ris2.getText());
            responso = 2;
            propertyChangeSupport.firePropertyChange("responso", 0, 2);
        });

        domanda.setText(storyNode.getDomanda());
        ris1.setText(storyNode.getScelta1());
        ris2.setText(storyNode.getScelta2());
    }

    private void _initFont() {
        try {
            stampact = Resource.getResource(Font.class, "Resources/Stampact.TTF").deriveFont(30f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(stampact);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            goldenage = Resource.getResource(Font.class, "Resources/Bohemian_Typewriter.ttf").deriveFont(25f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(goldenage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public int getResponso() {
        return responso;
    }

    public StoryNode getStoryNode() {
        return storyNode;
    }
}